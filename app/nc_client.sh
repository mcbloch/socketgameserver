#!/bin/bash

echo "Enter your message as a string in json syntax:"

read MESSAGE

# Needed for string length in bytes
LC_ALL=C

LENGTH=${#MESSAGE}


echo "String is ${LENGTH} long"

LENHEX=$(printf "%04x", $LENGTH)

# take the first 2 chars, then the other 2
HEX1=("${LENHEX[@]:0:2}")
HEX2=("${LENHEX[@]:2:2}")

echo $HEX1
echo $HEX2

DATA="'\x$HEX1\x$HEX2''$MESSAGE'"
echo $DATA

#TODO how do these single quote thingies work
# it should result in this:
# echo -e '\x00\x25''{"action": "search", "value": "ring"}' | nc 192.168.2.85 65431


# echo -e $DATA | nc 192.168.2.85 65431
