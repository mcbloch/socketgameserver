# Socket Game Server

This project is meant to function as server that 
- collects player data of all connected users and 
- on a frequent basis syncs this information to all these players

This creates a free to join open lobby where everyone can see the other player.

The app-client currently does not work with the server implementation as it isn't actively used and thus updated.

The server listens on a HOST and PORT to socket connection and will interact with these based on the infomation in the lobby.py file

## Examples

Example using netcat

    $ echo -e '\x00''\x25''{"action": "search", "value": "ring"}' | nc 192.168.2.85 65431
